import React, { useState, useEffect, useContext, Fragment } from 'react';
import { Howl } from 'howler';
import Styled from 'styled-components';
import config from 'visual-config-exposer';

import QuizCards from '../quizCards/quizCards';
import { GameContext } from '../../context/gameContext';
import { ScoreContext } from '../../context/scoreContext';
import SoundOn from '../../../public/assets/volume-2.svg';
import SoundOff from '../../../public/assets/volume-x.svg';
import './quiz.css';

const Container = Styled.div`
background-image: url(${config.quizSettings.quizBackground});
background-size: cover;
background-repeat: no-repeat;
background-position: center;
`;

const backSound = config.quizSettings.backgroundSound;
const endSound = config.quizSettings.endSound;

let startTime, endTime, time, interval;

const Quiz = (props) => {
  const gameContext = useContext(GameContext);
  const scoreContext = useContext(ScoreContext);
  let image = props.soundOn ? SoundOn : SoundOff;
  const [timeUp, setTimeUp] = useState(false);
  const [completed, setCompleted] = useState(false);

  let bkSound = new Howl({
    src: [backSound],
    loop: true,
  });

  let edSound = new Howl({
    src: [endSound],
  });

  useEffect(() => {
    startTime = new Date();
    interval = setInterval(quizEndHandler, 1000);
  }, []);

  useEffect(() => {
    if (props.soundOn) {
      bkSound.play();
    }

    return () => {
      if (bkSound) {
        bkSound.stop();
        bkSound.unload();
        bkSound = null;
      }
    };
  });

  const quizEndHandler = (status) => {
    endTime = new Date();
    time = (endTime - startTime) / 1000;
    console.log(status);
    if (status === 'COMPLETED') {
      setCompleted(true);
      if (bkSound) {
        bkSound.stop();
        bkSound.unload();
        bkSound = null;
      }
      clearInterval(interval);
      edSound.play();
      setTimeout(() => {
        gameContext.setPostScreen();
      }, 3000);
    } else if (time >= config.quizSettings.quizTime) {
      clearInterval(interval);
      if (bkSound) {
        bkSound.stop();
        bkSound.unload();
        bkSound = null;
      }
      edSound.play();
      setTimeout(() => {
        gameContext.setPostScreen();
      }, 3000);
      setTimeUp(true);
    }
  };

  return (
    <Fragment>
      {!completed && !timeUp ? (
        <Fragment>
          {config.quizSettings.showScore && (
            <div className="score">Score : {scoreContext.score}</div>
          )}
          <Container className="quiz__container">
            <div className="sound__image" onClick={props.soundHandler}>
              <img src={image} alt="sound" />
            </div>
            <QuizCards quizEnd={quizEndHandler} />
          </Container>
        </Fragment>
      ) : completed ? (
        <Container className="quiz__container">
          <h1>Quiz Completed</h1>
          <h1>Score : {scoreContext.score}</h1>
        </Container>
      ) : (
        <Container className="quiz__container">
          <h1>Time Up </h1>
          <h1>Score : {scoreContext.score}</h1>
        </Container>
      )}
    </Fragment>
  );
};

export default Quiz;

// {!timeUp ? <h1>Quiz Screen</h1> : <h1>Time Up</h1>}
